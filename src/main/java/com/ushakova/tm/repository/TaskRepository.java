package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.exception.entity.EntityNotFoundException;
import com.ushakova.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String projectId, @NotNull final String userId) {
        @Nullable List<Task> list = findAll(userId);
        Optional.ofNullable(list).orElseThrow(EntityNotFoundException::new);
        return list.stream()
                .filter(t -> t.getProjectId().equals(projectId))
                .collect(Collectors.toList());
    }

}

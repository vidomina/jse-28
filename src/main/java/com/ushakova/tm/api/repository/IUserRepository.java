package com.ushakova.tm.api.repository;

import com.ushakova.tm.api.IRepository;
import com.ushakova.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    List<User> findAll();

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User removeByLogin(@NotNull String login);

    @Nullable
    User removeUser(@NotNull User user);

}

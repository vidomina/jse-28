package com.ushakova.tm.exception.system;

import com.ushakova.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(@NotNull String command) {
        super("***Error: Command " + command + " Not Found.***");
    }

}
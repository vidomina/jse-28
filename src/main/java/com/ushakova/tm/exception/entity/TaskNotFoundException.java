package com.ushakova.tm.exception.entity;

import com.ushakova.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("An error has occurred: task not found.");
    }

}

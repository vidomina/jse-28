package com.ushakova.tm.command.data;

import com.ushakova.tm.command.AbstractDataCommand;
import com.ushakova.tm.dto.Domain;
import com.ushakova.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String description() {
        return "Load data from xml file by JAXB.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory
                .createContext(new Class[]{Domain.class}, null);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    public @NotNull String name() {
        return "data-load-xml-jaxb";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}


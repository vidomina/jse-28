package com.ushakova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ushakova.tm.command.AbstractDataCommand;
import com.ushakova.tm.dto.Domain;
import com.ushakova.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String description() {
        return "Load data from json by Faster XML.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    public @NotNull String name() {
        return "data-load-json-fasterxml";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}

package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectCompleteProjectByIndexCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Set \"Complete\" status to project by index.";
    }

    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Set Status \"Complete\" to Project***\nEnter Index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Project project = serviceLocator.getProjectService().completeByIndex(index, userId);
    }

    @Override
    @NotNull
    public String name() {
        return "complete-project-by-index";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}

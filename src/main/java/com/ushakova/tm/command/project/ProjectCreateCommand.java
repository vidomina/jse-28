package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Create new project.";
    }

    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Project Create***");
        System.out.println("Enter Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Project project = serviceLocator.getProjectService().add(name, description, userId);
    }

    @Override
    @NotNull
    public String name() {
        return "project-create";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
